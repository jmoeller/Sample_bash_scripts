#! /bin/bash
#======================================================================
#PBS -N baf2ns7.@startirr@.@lastirr@
#PBS -l walltime=11:59:00 
#PBS -l nodes=2:ppn=12
#PBS -q consort
#======================================================================

# source environment variables
if test "`whoami`" = "moeller" ; # if whoami yields moeller (which it doesn on cmlx14 and zahir) 
   then . /master_scratch/moeller/qevars; 
	else . $HOME/.qevars; 
fi 

cd $PBS_O_WORKDIR

cd .. 
WD=`pwd`

#============================ Files and Input =========================

WIT="baf2.rlxd.n.site7"

TMP_DIR=$SCRATCH_DIR/baf2/sc2h2g/$WIT

startirr=@startirr@
lastirr=@lastirr@

#======================================================================

# check whether ECHO has the -e option
if test "`echo -e`" = "-e" ; then ECHO=echo ; else ECHO="echo -e" ; fi

date 

$ECHO "  checking that needed directories and files exist...\c" 

for DIR in "$TMP_DIR" "$WD/phonon/$WIT" ; do
    if test ! -d $DIR ; then
	mkdir -p $DIR
    fi
done

#======================================================================

# copying required files
mkdir $TMP_DIR/startirr$startirr
cp -r $TMP_DIR/baf2.* $TMP_DIR/startirr$startirr
mkdir -p $TMP_DIR/startirr$startirr/_ph0/baf2.phsave
cp -r $TMP_DIR/_ph0/baf2.phsave/* $TMP_DIR/startirr$startirr/_ph0/baf2.phsave

# Change to data directory
cd $WD/phonon/$WIT

# phonon calculation at Gamma
cat > $WIT.grid.$startirr.$lastirr.ph.in << EOF
phonons at gamma
 &inputph
  tr2_ph=1.0d-14,
  prefix='baf2',
  epsil=.false.,
  amass(1)=137.327  
  amass(2)=18.9984 
  amass(3)=0.11343 
  start_irr=$startirr,
  last_irr=$lastirr,  
  recover=.true.
  outdir='$TMP_DIR/startirr$startirr',
  fildyn='$WIT.dyn',
  search_sym=.false.
  max_seconds=42000
 /
0.0 0.0 0.0
EOF

$ECHO "  running the phonon calculation for $startirr to $lastirr ...\c"
$PH_COMMAND < $WIT.grid.$startirr.$lastirr.ph.in >> $WIT.grid.$startirr.$lastirr.ph.out
$ECHO " done"

date 
