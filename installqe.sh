#! /bin/bash

echo "setting up quantum espresso ... "
mkdir -p ~/code/espresso
cd ~/code/espresso
wget http://www.qe-forge.org/gf/download/frsrelease/116/403/espresso-5.0.2.tar.gz
tar zxvf espresso-5.0.2.tar.gz
rm espresso-5.0.2.tar.gz
cp -r espresso-5.0.2 espresso-5.0.3
cd espresso-5.0.3

wget http://www.qe-forge.org/gf/download/frsrelease/128/435/espresso-5.0.2-5.0.3.diff
patch -p1 < espresso-5.0.2-5.0.3.diff

module load openmpi/1.3.3/intel intel/compilers/11.1 fftw/3.2.2/intel/double intel/mkl/10.2.1
# on sal:
#module load intel-compilers/11.1 openmpi/1.4.2__intel-11.1 intel-mkl/11.1

echo "configuring"
./configure LIBDIRS=/local/software/rh53/fftw/3.2.2/intel/double/lib

echo "compiling"
make all

cd ..
ln -s espresso-5.0.3 current

cd 
echo "fetching .qevars"

wget https://dl.dropbox.com/u/1555183/.qevars

mkdir -p ~/jobs/scratch
mkdir -p ~/espresso/tools 

echo "fetching pseudopotentials"
cd ~/code/espresso
wget https://dl.dropbox.com/u/1555183/pseudo.tgz
tar zxvf pseudo.tgz 
rm pseudo.tgz

cd 
echo "all done!"