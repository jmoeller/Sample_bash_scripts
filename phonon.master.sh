#!/bin/sh

for startirr in 254 273; do # modes 254-291
lastirr="`echo "$startirr + 18" | bc`"

# make the scripts
cat phonon.n.site7.grid.tmpl.sh | sed "s/@startirr@/$startirr/" | sed "s/@lastirr@/$lastirr/" > phonon.n.site7.grid.$startirr.$lastirr.sh

# submit them 
qsub phonon.n.site7.grid.$startirr.$lastirr.sh

# wait a moment (in sec) 
sleep 1

done

for startirr in `seq 2 18 236`; do # modes 1-253 inclusive
lastirr="`echo "$startirr + 17" | bc`"

# make the scripts
cat phonon.n.site7.grid.tmpl.sh | sed "s/@startirr@/$startirr/" | sed "s/@lastirr@/$lastirr/" > phonon.n.site7.grid.$startirr.$lastirr.sh

# submit them 
qsub phonon.n.site7.grid.$startirr.$lastirr.sh

# wait a moment (in sec) 
sleep 1

done