This is a typical example of the scripts I have written to control jobs 
on a high-performance computing cluster. This serves as an illustration only 
as the calculation itself requires the main program "Quantum Espresso"
(http://www.quantum-espresso.org/). 

This example shows how lattice vibrations "phonons" in a crystal can be 
calculated using Quantum Espresso. The calculation is computationally very demanding.
Therefore it has been heavily parallelized: each job only deals with part of the 
calculation for so-called irreps (9 irreps per job, run sequentially) on 256 processors 
per job (openmpi). 
The results of these calculations are then combined afterwards to yield the final 
complete lattice vibration spectrum. In this case the calculation cost about 
40h/9 irreps x 256 CPUs x 291 irreps in total = 330.000 CPUh.

The scientific motivation is explained at: https://arxiv.org/abs/1212.6782

Brief inventory:
installqe.sh: my installtion script to install Quantum Espresso on various different clusters,
phonon.ini.sh: initializes the calculation,
phonon.tmpl.sh: template that contains generic instructions for each of the separate jobs,
phonon.master.sh: takes the template and creates individual jobs,
phonon.final.sh: pulls the individual results together and creates the output.
