#! /bin/bash
#======================================================================
#PBS -N baf2ns7.final
#PBS -l walltime=0:59:00 
#PBS -l nodes=2:ppn=12
#PBS -q consort
#PBE -m bea
#PBE -M j.moeller1@physics.ox.ac.uk
#======================================================================

# source environment variables
if test "`whoami`" = "moeller" ; # if whoami yields moeller (which it doesn on cmlx14 and zahir) 
   then . /master_scratch/moeller/qevars; 
	else . $HOME/.qevars; 
fi 

cd $PBS_O_WORKDIR

cd .. 
WD=`pwd`

#============================ Files and Input =========================

WIT="baf2.rlxd.n.site7"

TMP_DIR=$SCRATCH_DIR/baf2/sc2h2g/$WIT
#======================================================================

# check whether ECHO has the -e option
if test "`echo -e`" = "-e" ; then ECHO=echo ; else ECHO="echo -e" ; fi

date 

$ECHO "  checking that needed directories and files exist...\c" 

for DIR in "$TMP_DIR" "$WD/phonon/$WIT" ; do
    if test ! -d $DIR ; then
	mkdir -p $DIR
    fi
done

#======================================================================
# copy required tmp files (only xml files, shouldn't be > ~ 15 MB per startirr)

cp $TMP_DIR/startirr*/_ph0/baf2.phsave/data-file*.xml $TMP_DIR/_ph0/baf2.phsave

#======================================================================
# Change to data directory
cd $WD/phonon/$WIT

# final phonon calculation to collect results
cat > $WIT.grid.$startirr.$lastirr.ph.in << EOF
phonons at gamma
 &inputph
  tr2_ph=1.0d-14,
  prefix='baf2',
  epsil=.false.,
  amass(1)=137.327  
  amass(2)=18.9984 
  amass(3)=0.11343 
  recover=.true.
  outdir='$TMP_DIR/startirr$startirr',
  fildyn='$WIT.dyn',
  search_sym=.false.
  max_seconds=3000
 /
0.0 0.0 0.0
EOF

$ECHO "  running the phonon calculation for ...\c"
$PH_COMMAND < $WIT.ph.final.in > $WIT.ph.final.out
$ECHO " done"

# ASR and postprocssing
cat > $WIT.dynmat.in <<EOF
 &input
    fildyn = '$WIT.dyn'
    filmol = '$WIT.mold'
    filxsf = '$WIT.axsf'
    filout = '$WIT.dynmat'
    asr='crystal',    
 /
EOF

$ECHO "  recalculating omega(q) from C(R) now with ASR ...\c"
$DYNMAT_COMMAND < $WIT.dynmat.in > $WIT.dynmat.out
$ECHO " done"

cat > $WIT.dynmat.simple.in <<EOF
 &input
    fildyn = '$WIT.dyn'
    filmol = '$WIT.simple.mold'
    filxsf = '$WIT.simple.axsf'
    filout = '$WIT.simple.dynmat'
    asr='simple',    
 /
EOF

$ECHO "  recalculating omega(q) from C(R) now with ASR ...\c"
$DYNMAT_COMMAND < $WIT.dynmat.simple.in > $WIT.dynmat.simple.out
$ECHO " done"


date 
